﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.Ajax.Utilities;
using s15439.MAS_API.Context;
using s15439.MAS_API.DTOs;
using s15439.MAS_API.Models;
using s15439.MAS_API.Repositories;

namespace s15439.MAS_API.Controllers
{
    public class UsersController : ApiController
    {
        private readonly UsersRepository rep = new UsersRepository();

        // GET: api/Users
        public IQueryable<UserDTO> GetUsers()
        {

            var users = rep.GetUsers();


            return users;
        }

        // GET: api/Users/5
        [ResponseType(typeof(User))]
        public IHttpActionResult GetUser(int id)
        {
            var user = rep.GetUser(id);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/Users/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUser(int id, User user)
        {
            throw new NotImplementedException();
        }

        // POST: api/Users
        [ResponseType(typeof(User))]
        public IHttpActionResult PostUser(User user)
        {
            throw new NotImplementedException();
        }

        // DELETE: api/Users/5
        [ResponseType(typeof(User))]
        public IHttpActionResult DeleteUser(int id)
        {
            throw new NotImplementedException();
        }


        private bool UserExists(int id)
        {
            throw new NotImplementedException();
        }
    }
}
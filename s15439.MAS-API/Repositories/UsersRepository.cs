﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using s15439.MAS_API.Context;
using s15439.MAS_API.DTOs;

namespace s15439.MAS_API.Repositories
{
    public class UsersRepository
    {
        private SpaceContext db = new SpaceContext();

        public IQueryable<UserDTO> GetUsers()
        {
            var users = db.Users.Include("Stations").Include("Ships")
                .Select(d => new UserDTO
                {
                    Username = d.Username,
                    UserId = d.UserId,
                    CreditsAmount = d.CreditsAmount,
                    Faction = d.Faction.ToString(),
                    PictureURL = d.PictureURl,
                    CurrentLocation = new StationDTO
                    {
                        StationId = d.CurrentLocation.StationId,
                        Name = d.CurrentLocation.Name,
                        Affiliation = d.CurrentLocation.Affiliation.ToString()
                    },
                    Ship = new ShipForUserDTO()
                    {
                        Callsign = d.Ship.Callsign,
                        SerialNumber = d.Ship.SerialNumber,
                        ShipModel = d.Ship.Model.ModelName,
                        Capacity = d.Ship.Capacity,
                        Speed = d.Ship.Speed,
                        Level = d.Ship.Level,
                        UsedCapacity = 0
                    }
                });


            return users;
        }

        public UserDTO GetUser(int id)
        {
            var user = db.Users.Include("Stations")
                .Select(d => new UserDTO
                {
                    Username = d.Username,
                    UserId = d.UserId,
                    CreditsAmount = d.CreditsAmount,
                    Faction = d.Faction.ToString(),
                    PictureURL = d.PictureURl,
                    CurrentLocation = new StationDTO
                    {
                        StationId = d.CurrentLocation.StationId,
                        Name = d.CurrentLocation.Name,
                        Affiliation = d.CurrentLocation.Affiliation.ToString()
                    },
                    Ship = new ShipForUserDTO()
                    {
                        Callsign = d.Ship.Callsign,
                        SerialNumber = d.Ship.SerialNumber,
                        ShipModel = d.Ship.Model.ModelName,
                        Capacity = d.Ship.Capacity,
                        Speed = d.Ship.Speed,
                        Level = d.Ship.Level,
                        UsedCapacity = 0
                    }
                }).First();


            return user;
        }
    }
}
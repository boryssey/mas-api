﻿using s15439.MAS_API.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace s15439.MAS_API.Context
{
    class SpaceContext : DbContext
    {
        public SpaceContext() : base("MAS-project")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SpaceContext, Migrations.Configuration>());
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Coordinates> Cooridantes{ get; set; }
        public DbSet<Item> Items{ get; set; }
        public DbSet<ItemsAmount> ItemsAmounts { get; set; }
        public DbSet<Market> Markets{ get; set; }
        public DbSet<Ship> Ships { get; set; }
        public DbSet<ShipModel> ShipModels { get; set; }
        public DbSet<Station> Stations { get; set; }
        public DbSet<UserTraveling> UserTravelings { get; set; }


    }
}
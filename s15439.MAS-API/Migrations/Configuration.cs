using s15439.MAS_API.Models;

namespace s15439.MAS_API.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<s15439.MAS_API.Context.SpaceContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(s15439.MAS_API.Context.SpaceContext context)
        {
            var coord = new Coordinates(1, 1, 1);
            var coord2 = new Coordinates(2, 0, 1);
            var planetSys = new PlanetSystem(1, "Lecaio", coord);
            var planetSys1 = new PlanetSystem(2, "Pecruvis", coord2);

            var station = new Station(1, "Kuru", planetSys, Station.StationType.PirateStation);
            var station1 = new Station(2, "Thetune", planetSys, Station.StationType.FederationStation);
            var station2 = new Station(3, "Laphoth", planetSys, Station.StationType.NeutralStation);
            var station3 = new Station(4, "Padaestea", planetSys1, Station.StationType.NeutralStation);
            var station4 = new Station(5, "Yuhines", planetSys1, Station.StationType.FederationStation);
            context.Stations.AddOrUpdate(station);
            context.Stations.AddOrUpdate(station2);
            context.Stations.AddOrUpdate(station1);
            context.Stations.AddOrUpdate(station3);
            context.Stations.AddOrUpdate(station4);
            var user = new User(2, Models.User.UserType.Pirate, "boryssey", 1500, station,
                "https://i.imgur.com/PpPARxH.jpg");
            var shipModel = new ShipModel(1, "Speedy", 1500);
            var ship = new Ship(1, "SPD-1000", shipModel, 500, 1, 1, user);
            context.Ships.AddOrUpdate(ship);
            context.Users.AddOrUpdate(user);
            context.SaveChanges();
        }
    }
}
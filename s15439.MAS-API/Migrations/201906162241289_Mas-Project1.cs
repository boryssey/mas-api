namespace s15439.MAS_API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MasProject1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Coordinates",
                c => new
                    {
                        CoordinatesId = c.Int(nullable: false, identity: true),
                        CoordinateX = c.Double(nullable: false),
                        CoordinateY = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.CoordinatesId);
            
            CreateTable(
                "dbo.PlanetSystems",
                c => new
                    {
                        PlanetSystemId = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Coordinates_CoordinatesId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PlanetSystemId)
                .ForeignKey("dbo.Coordinates", t => t.Coordinates_CoordinatesId)
                .Index(t => t.Coordinates_CoordinatesId);
            
            CreateTable(
                "dbo.Stations",
                c => new
                    {
                        StationId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Affiliation = c.Int(nullable: false),
                        CurrentMultiplier = c.Double(nullable: false),
                        PlanetSystem_PlanetSystemId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.StationId)
                .ForeignKey("dbo.PlanetSystems", t => t.PlanetSystem_PlanetSystemId, cascadeDelete: true)
                .Index(t => t.PlanetSystem_PlanetSystemId);
            
            CreateTable(
                "dbo.Markets",
                c => new
                    {
                        MarketId = c.Int(nullable: false, identity: true),
                        Station_StationId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.MarketId)
                .ForeignKey("dbo.Stations", t => t.Station_StationId)
                .Index(t => t.Station_StationId);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        ItemId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Rarity = c.Int(nullable: false),
                        StockPrice = c.Int(nullable: false),
                        Weight = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ItemId);
            
            CreateTable(
                "dbo.ItemsAmounts",
                c => new
                    {
                        ItemsAmountId = c.Int(nullable: false, identity: true),
                        Amount = c.Int(nullable: false),
                        Item_ItemId = c.Int(nullable: false),
                        Ship_SerialNumber = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.ItemsAmountId)
                .ForeignKey("dbo.Items", t => t.Item_ItemId, cascadeDelete: true)
                .ForeignKey("dbo.Ships", t => t.Ship_SerialNumber, cascadeDelete: true)
                .Index(t => t.Item_ItemId)
                .Index(t => t.Ship_SerialNumber);
            
            CreateTable(
                "dbo.Ships",
                c => new
                    {
                        SerialNumber = c.Long(nullable: false, identity: true),
                        Callsign = c.String(nullable: false),
                        Capacity = c.Int(nullable: false),
                        Speed = c.Int(nullable: false),
                        Level = c.Int(nullable: false),
                        Model_ShipModelId = c.Int(nullable: false),
                        Owner_UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SerialNumber)
                .ForeignKey("dbo.ShipModels", t => t.Model_ShipModelId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.Owner_UserId)
                .Index(t => t.Model_ShipModelId)
                .Index(t => t.Owner_UserId);
            
            CreateTable(
                "dbo.ShipModels",
                c => new
                    {
                        ShipModelId = c.Int(nullable: false, identity: true),
                        ModelName = c.String(nullable: false),
                        StockPrice = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ShipModelId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Faction = c.Int(nullable: false),
                        Username = c.String(nullable: false),
                        CreditsAmount = c.Int(nullable: false),
                        CurrentLocation_StationId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Stations", t => t.CurrentLocation_StationId, cascadeDelete: true)
                .Index(t => t.CurrentLocation_StationId);
            
            CreateTable(
                "dbo.UserTravelings",
                c => new
                    {
                        UserTravelingId = c.Int(nullable: false, identity: true),
                        TravelTime = c.Long(nullable: false),
                        IsTraveling = c.Boolean(nullable: false),
                        Marauding = c.Boolean(nullable: false),
                        Destination_StationId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.UserTravelingId)
                .ForeignKey("dbo.Stations", t => t.Destination_StationId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserTravelingId)
                .Index(t => t.UserTravelingId)
                .Index(t => t.Destination_StationId);
            
            CreateTable(
                "dbo.ShipModelMarkets",
                c => new
                    {
                        ShipModel_ShipModelId = c.Int(nullable: false),
                        Market_MarketId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ShipModel_ShipModelId, t.Market_MarketId })
                .ForeignKey("dbo.ShipModels", t => t.ShipModel_ShipModelId, cascadeDelete: true)
                .ForeignKey("dbo.Markets", t => t.Market_MarketId, cascadeDelete: true)
                .Index(t => t.ShipModel_ShipModelId)
                .Index(t => t.Market_MarketId);
            
            CreateTable(
                "dbo.ItemMarkets",
                c => new
                    {
                        Item_ItemId = c.Int(nullable: false),
                        Market_MarketId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Item_ItemId, t.Market_MarketId })
                .ForeignKey("dbo.Items", t => t.Item_ItemId, cascadeDelete: true)
                .ForeignKey("dbo.Markets", t => t.Market_MarketId, cascadeDelete: true)
                .Index(t => t.Item_ItemId)
                .Index(t => t.Market_MarketId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Stations", "PlanetSystem_PlanetSystemId", "dbo.PlanetSystems");
            DropForeignKey("dbo.Markets", "Station_StationId", "dbo.Stations");
            DropForeignKey("dbo.ItemMarkets", "Market_MarketId", "dbo.Markets");
            DropForeignKey("dbo.ItemMarkets", "Item_ItemId", "dbo.Items");
            DropForeignKey("dbo.ItemsAmounts", "Ship_SerialNumber", "dbo.Ships");
            DropForeignKey("dbo.Ships", "Owner_UserId", "dbo.Users");
            DropForeignKey("dbo.UserTravelings", "UserTravelingId", "dbo.Users");
            DropForeignKey("dbo.UserTravelings", "Destination_StationId", "dbo.Stations");
            DropForeignKey("dbo.Users", "CurrentLocation_StationId", "dbo.Stations");
            DropForeignKey("dbo.Ships", "Model_ShipModelId", "dbo.ShipModels");
            DropForeignKey("dbo.ShipModelMarkets", "Market_MarketId", "dbo.Markets");
            DropForeignKey("dbo.ShipModelMarkets", "ShipModel_ShipModelId", "dbo.ShipModels");
            DropForeignKey("dbo.ItemsAmounts", "Item_ItemId", "dbo.Items");
            DropForeignKey("dbo.PlanetSystems", "Coordinates_CoordinatesId", "dbo.Coordinates");
            DropIndex("dbo.ItemMarkets", new[] { "Market_MarketId" });
            DropIndex("dbo.ItemMarkets", new[] { "Item_ItemId" });
            DropIndex("dbo.ShipModelMarkets", new[] { "Market_MarketId" });
            DropIndex("dbo.ShipModelMarkets", new[] { "ShipModel_ShipModelId" });
            DropIndex("dbo.UserTravelings", new[] { "Destination_StationId" });
            DropIndex("dbo.UserTravelings", new[] { "UserTravelingId" });
            DropIndex("dbo.Users", new[] { "CurrentLocation_StationId" });
            DropIndex("dbo.Ships", new[] { "Owner_UserId" });
            DropIndex("dbo.Ships", new[] { "Model_ShipModelId" });
            DropIndex("dbo.ItemsAmounts", new[] { "Ship_SerialNumber" });
            DropIndex("dbo.ItemsAmounts", new[] { "Item_ItemId" });
            DropIndex("dbo.Markets", new[] { "Station_StationId" });
            DropIndex("dbo.Stations", new[] { "PlanetSystem_PlanetSystemId" });
            DropIndex("dbo.PlanetSystems", new[] { "Coordinates_CoordinatesId" });
            DropTable("dbo.ItemMarkets");
            DropTable("dbo.ShipModelMarkets");
            DropTable("dbo.UserTravelings");
            DropTable("dbo.Users");
            DropTable("dbo.ShipModels");
            DropTable("dbo.Ships");
            DropTable("dbo.ItemsAmounts");
            DropTable("dbo.Items");
            DropTable("dbo.Markets");
            DropTable("dbo.Stations");
            DropTable("dbo.PlanetSystems");
            DropTable("dbo.Coordinates");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using s15439.MAS_API.Models;

namespace s15439.MAS_API.DTOs
{
    public class ShipForUserDTO
    {
        public string Callsign { get; set; }
        public long SerialNumber { get; set; }
        public string ShipModel { get; set; }
        public int Capacity { get; set; }
        public int Speed { get; set; }
        public int Level{ get; set; }
        public int UsedCapacity { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using s15439.MAS_API.DTOs;
using s15439.MAS_API.Models;

namespace s15439.MAS_API
{
    public class UserDTO
    {
        public string Username { get; set; }
        public int UserId { get; set; }
        public int CreditsAmount { get; set; }
        public string Faction { get; set; }
        public string PictureURL { get; set; }  

        public StationDTO CurrentLocation { get; set; }
        public ShipForUserDTO Ship { get; set; }
    }
}
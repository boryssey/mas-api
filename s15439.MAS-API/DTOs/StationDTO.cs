﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace s15439.MAS_API.DTOs
{
    public class StationDTO
    {
        public long StationId { get; set; }
        public string Name { get; set; }
        public string Affiliation { get; set; }

        
    }
}
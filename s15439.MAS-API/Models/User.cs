﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace s15439.MAS_API.Models
{
    public class User
    {
        public enum UserType
        {
            Traveler,
            Trader,
            Pirate
        }
        public User()
        {

        }
        public User(UserType faction, string username, int CreditsAmount, Station Location)
        {
            this.Faction = faction;
            this.Username = username;
            this.CreditsAmount = CreditsAmount;
            this.CurrentLocation = Location;
        }

        public User(int userId, UserType faction, string username, int CreditsAmount, Station Location, string pictureUrl)
        {
            this.UserId = userId;
            this.Faction = faction;
            this.Username = username;
            this.CreditsAmount = CreditsAmount;
            this.CurrentLocation = Location;
            PictureURl = pictureUrl;
        }
        public static string convertToString(Enum eff)
        {
            return Enum.GetName(eff.GetType(), eff);
        }
        public static UserType converToEnum<EnumTUserTypeype>(string enumValue)
        {
            return (UserType)Enum.Parse(typeof(UserType), enumValue);
        }
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int UserId { get; set; }

        [Required]
        public UserType Faction { get; set; }
        [Required]
        public string Username { get; set; }
        public int CreditsAmount { get; set; }
        public string PictureURl { get; set; }
        [Required]
        public Station CurrentLocation { get; set; }
        public UserTraveling Travel { get; set; }
        
        public virtual Ship Ship { get; set; }
        public static double SellingMultiplier { get; set; } = 1.2;

        public void Maraud(User User)
        {

        }

        public long CalculateTimeToStation(Station Station)
        {


            return 0L;
        }

        public void SellShip()
        {

        }
        public void BuyShip(ShipModel Model)
        {

        }
        public void BecomeTrader()
        {

        }
        public void BecomePirate()
        {

        }
        public void BecomeTraveler()
        {

        }

        public override string ToString()
        {
            return $"{nameof(UserId)}: {UserId}, {nameof(Faction)}: {Faction}, {nameof(Username)}: {Username}, {nameof(CreditsAmount)}: {CreditsAmount}, {nameof(CurrentLocation)}: {CurrentLocation}";
        }
    }
}
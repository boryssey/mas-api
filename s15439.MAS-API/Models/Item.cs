﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace s15439.MAS_API.Models
{
    public enum ItemRarity
    {
        Common,
        Rare,
        UltraRare
    }
    public class Item
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int ItemId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public ItemRarity Rarity{ get; set; }
        [Required]
        public int StockPrice { get; set; }
        [Required]
        public int Weight { get; set; }
        public ICollection<Market> Markets { get; set; }
        public ICollection<ItemsAmount> ItemsAmounts { get; set; }




    }
}
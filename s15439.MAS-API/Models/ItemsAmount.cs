﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace s15439.MAS_API.Models
{
    public class ItemsAmount
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int ItemsAmountId { get; set; }
        [Required]
        public Ship Ship { get; set; }
        [Required]
        public int Amount { get; set; }
        [Required]
        public Item Item { get; set; }
    }
}
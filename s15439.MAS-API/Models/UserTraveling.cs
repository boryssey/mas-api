﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace s15439.MAS_API.Models
{
    public class UserTraveling
    {
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int UserTravelingId { get; set; }

        [Required]
        public long TravelTime { get; set; }

        [Required]
        public bool IsTraveling { get; set; } = true;

        [Required]
        public bool Marauding { get; set; } = false;

        [Required]
        public Station Destination { get; set; }

        [Required]
        public User Traveler { get; set; }



    }
}
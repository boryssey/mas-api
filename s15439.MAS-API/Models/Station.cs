﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace s15439.MAS_API.Models
{
    public class Station
    {
        public enum StationType
        {
            PirateStation,
            FederationStation,
            NeutralStation

        }
        public Station()
        {

        }

        public Station(string name, PlanetSystem sys, StationType type)
        {
            this.PlanetSystem = sys;
            Name = name;
            Affiliation = type;
        }
        public Station(int StationId, string name, PlanetSystem sys, StationType type)
        {
            this.StationId = StationId;
            this.PlanetSystem = sys;
            Name = name;
            Affiliation = type;
        }
        public static string ConvertToString(Enum eff)
        {
            return Enum.GetName(eff.GetType(), eff);
        }

        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long StationId { get; set; }
        [Required]
        public string Name { get; set; }

        [Required]
        public StationType Affiliation { get; set; }
        [Required]
        public double CurrentMultiplier { get; set; } = 1;
        public static double MinMultiplier { get; set; } = 0.75;

        public static double MaxMultiplier { get; set; } = 1.25;

        public ICollection<User> UsersLocatedHere { get; set; }

        public ICollection<UserTraveling> TravelingHere { get; set; }

        [Required]
        public PlanetSystem PlanetSystem { get; set; }

        public Market Market { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace s15439.MAS_API.Models
{
    public class Ship
    {
        public Ship()
        {

        }

        public Ship(long serialNumber, string callsign, ShipModel model, int capacity, int speed, int level, User owner)
        {
            SerialNumber = serialNumber;
            Callsign = callsign;
            Model = model;
            Capacity = capacity;
            Speed = speed;
            Level = level;
            Owner = owner;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public long SerialNumber { get; set; }
        [Required]
        public string Callsign { get; set; }
        [Required]
        public ShipModel Model { get; set; }
        [Required]
        public int Capacity { get; set; }
        [Required]
        public int Speed { get; set; }
        [Required]
        public int Level { get; set; }
        public static int MaxLevel { get; } = 10;
        public static int MinLevel { get; } = 1;

        [Required]
        public User Owner { get; set; }
        public ICollection<ItemsAmount> ItemsAmounts { get; set; }


    }
}
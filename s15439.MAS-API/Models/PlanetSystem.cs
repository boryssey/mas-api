﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace s15439.MAS_API.Models
{
    public class PlanetSystem
    {

        public PlanetSystem()
        {

        }

        public PlanetSystem(string name, Coordinates coord)
        {
            Coordinates = coord;
            Name = name;
        }
        public PlanetSystem(long id, string name, Coordinates coord)
        {
            PlanetSystemId = id;
            Coordinates = coord;
            Name = name;
        }
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long PlanetSystemId { get; set; }
        public string Name { get; set; }
        [Required]
        public Coordinates Coordinates { get; set; }

        public ICollection<Station> Stations{ get; set; }

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}, {nameof(Coordinates)}: {Coordinates}";
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace s15439.MAS_API.Models
{
    public class ShipModel
    {
        public ShipModel()
        {

        }

        public ShipModel(int shipModelId, string modelName, int stockPrice)
        {
            ShipModelId = shipModelId;
            ModelName = modelName;
            StockPrice = stockPrice;
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        [Key]
        public int ShipModelId { get; set; }
        [Required]
        public string ModelName { get; set; }
        [Required]
        public int StockPrice { get; set; }
        public ICollection<Ship> Ships { get; set; }
        public ICollection<Market> Markets { get; set; }
    }
}
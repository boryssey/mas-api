﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace s15439.MAS_API.Models
{
    public class Coordinates
    {
        public Coordinates()
        {
        }

        public Coordinates(double x, double y)
        {
            CoordinateX = x;
            CoordinateY = y;
        }
        public Coordinates(int id,double x, double y)
        {
            CoordinatesId = id;
            CoordinateX = x;
            CoordinateY = y;
        }

        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CoordinatesId { get; set; }
        [Required]
        public double CoordinateX { get; set; }
        [Required]
        public double CoordinateY { get; set; }
        public PlanetSystem PlanetSystem { get; set; }

        public override string ToString()
        {
            return $"{nameof(CoordinateX)}: {CoordinateX}, {nameof(CoordinateY)}: {CoordinateY}";
        }
    }
}
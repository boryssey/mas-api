﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace s15439.MAS_API.Models
{
    public class Market
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public int MarketId { get; set; }
        public HashSet<Item> AvailableItems { get; set; }
        public HashSet<ShipModel> AvailableShips { get; set; }
        [Required]
        public Station Station { get; set; }
    }
}